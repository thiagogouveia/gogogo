[ -z "$1" ] && L=1 || L=$1

F1="-O2 -Wconversion -Wshadow -Wfloat-equal"
F2="$F1 -fsanitize=address -Wunused-value"
F3="$F2 -fsanitize=undefined -fno-sanitize-recover -fstack-protector"
F4="$F3 -pedantic -std=c++17"
F5="$F4 -Wall -Wextra"

NO="-Wno-misleading-indentation"

[ $L -ge 5 ] && echo "$F5" && exit
[ $L -ge 4 ] && echo "$F4" && exit
[ $L -ge 3 ] && echo "$F3 $NO" && exit
[ $L -ge 2 ] && echo "$F2 $NO" && exit
[ $L -ge 1 ] && echo "$F1 $NO" && exit

