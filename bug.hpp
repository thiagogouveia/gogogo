
    #define bugL() { cerr << "*** ----------\n"; }

	#define bug(_x) { cerr << "*** " << #_x << " " << _x << '\n' << flush; }

	#define bug2(_x,_y) { \
        cerr << "*** " << #_x << " " << _x << " " \
                       << #_y << " " << _y << '\n' << flush; \
    }

	#define bug3(_x,_y, _z) { \
        cerr << "*** " << #_x << " " << _x << " " \
                       << #_y << " " << _y << " " \
                       << #_z << " " << _z << '\n' << flush; \
    }

	#define bugV(_V) { \
        cerr << "*** " << #_V << ": "; for( auto _v:_V) \
        cerr << _v << " "; cerr << '\n' << flush; \
    }

	#define bugP(_V) { \
        cerr << "*** " << #_V << ": | "; \
        for( auto [_i,_j]:_V) \
            cerr << _i << " " << _j << " | "; \
        cerr << '\n' << flush; \
    }
	