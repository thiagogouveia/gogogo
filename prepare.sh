#!/bin/bash

P=$1

[ -z "$P" ] && echo "\$1 must be the problem number, exiting..." && exit 1

# Template Version from t.cpp
function blue(){
	echo -e "\e[34m${1}\e[0m"
}

blue "$(head -n 1 t*.cpp)"

mkdir $P || exit 1
echo "Folder $P created successfully!"

echo "/* ** Criado em $(date +%H:%M:%S) ** */" > $P/t.cpp

for T in t*.cpp; do
	cat $T >> $P/t.cpp
	break;
done

cp bug.hpp $P/
cp flags.sh $P/

touch $P/0 $P/1 $P/2 $P/3
ln -s /dev/null $P/N


echo "Templates created successfully!"

cd $P

echo "Entering the folder (if you called source)..."
