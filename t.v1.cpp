/* *** Template v2.1 by GovBoy 05.01.2025 *** */

#include <bits/stdc++.h>
using namespace std;
using ll = long long;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count()); //mt19937_64

#ifdef EBUG
	#include "bug.hpp"
#else
	#define bug(x) { ; }
	#define bug2(x,y) { ; }
	#define bug3(x,y,z) { ; }
	#define bugV(V) { ; }
	#define bugP(V) { ; }
	#define bugL() { ; }
#endif

#define all(v) v.begin(), v.end()
#define esac(s) { cout << s << '\n'; return; }

#define INF (LLONG_MAX)

void solve(){

	int N; cin >> N;
	//string s; cin >> s; //int N = (int) s.size();
	vector<int> A(N); for( auto &x: A ) cin >> x;


	esac("YES");
}

signed main( void ){
	clock_t start_time = clock();
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);

	int T; cin >> T; while( T-- )
	solve();

	double total_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	bugL(); bug(total_time);
	return 0;
}
